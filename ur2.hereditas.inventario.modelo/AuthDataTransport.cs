﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class AuthDataTransport
    {
        public string username { get; set; }
        public string password { get; set; }

        public AuthDataTransport(string un, string pw)
        {
            username = un;
            password = pw;
        }

        public string Serializar()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
