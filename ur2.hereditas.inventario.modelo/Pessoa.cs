﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    [Serializable]
    public class Pessoa
    {
        public string pk { get; set; }
        public string rfid { get; set; }
        public string user { get; set; }
        public string nomecompleto { get; set; }
        public string login { get; set; }
        public bool Valido
        {
            get
            {
                return pk != null && user != null && login != null && (pk.Length > 0 && user.Length > 0 && login.Length > 0);
            }
        }

        public Pessoa() { }

        public Pessoa(string k)
        {
            pk = k;
        }

        public static void Deserializador(string json,ref List<Pessoa> lista)
        {
            lista = JsonConvert.DeserializeObject<List<Pessoa>>(json);
        }

    }
}
