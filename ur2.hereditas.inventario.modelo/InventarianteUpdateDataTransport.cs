﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class InventarianteUpdateDataTransport
    {
        public string rfid { get; set; }

        public InventarianteUpdateDataTransport(string r)
        {
            rfid = r;
        }

        public string Serializar()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
