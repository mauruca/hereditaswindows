﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    [Serializable]
    public class Local
    {
        public string pk { get; set; }
        public string nome { get; set; }
        public string rfid { get; set; }

        public Local() { }

        public Local(string k)
        {
            pk = k;
        }

        public static void Deserializador(string json, ref List<Local> lista)
        {
            lista = JsonConvert.DeserializeObject<List<Local>>(json);
        }
    }
}
