﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class InventarioDataTransport
    {
        public string data { get; set; }
        public string bem { get; set; }
        public string local { get; set; }
        public string user { get; set; }

        public InventarioDataTransport(string pdt, string pb, string pl, string pu)
        {
            data = pdt;
            bem = pb;
            local = pl;
            user = pu;
        }

        public string Serializar()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }
    }
}
