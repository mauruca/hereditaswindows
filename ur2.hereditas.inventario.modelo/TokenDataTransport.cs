﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class TokenDataTransport
    {
        public string token { get; set; }

        public static Token Deserializador(string json)
        {
            if (json.Trim().Length == 0)
                return null;
            return new Token(JsonConvert.DeserializeObject<TokenDataTransport>(json).token);
        }
    }
}
