﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    [Serializable]
    public class Bem
    {
        public string pk { get; set; }
        public string descricao { get; set; }
        public string rfid { get; set; }

        public Bem() { }

        public Bem(string k)
        {
            pk = k;
        }

        public static void Deserializador(string json,ref List<Bem> lista)
        {
            lista = JsonConvert.DeserializeObject<List<Bem>>(json);
        }
    }
}
