﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ur2.Hereditas.Inventario.Modelo
{
    [Serializable]
    public class InventarioBem
    {
        private DateTime _data;
        public DateTime Data { get { return _data; } }
        public List<Bem> Bens { get; set; }
        public Local Local { get; set; }
        public Pessoa Pessoa { get; set; }

        public bool Valido
        {
            get { return LocalValido() && UserValido() && BensValido(); }
        }

        private bool LocalValido()
        {
            return Local != null && Local.pk != null && Local.pk.Trim().Length > 0;
        }
        private bool UserValido()
        {
            return Pessoa != null && Pessoa.pk != null && Pessoa.pk.Trim().Length > 0;
        }
        private bool BensValido()
        {
            return Bens != null && Bens.Count > 0;
        }
        
        public InventarioBem()
        {
            _data = DateTime.Now;
            Bens = new List<Bem>();
        }

        public void AdicionaPessoa(object o)
        {
            if (o == null || !(o is Pessoa))
                return;
            Pessoa = (Pessoa)o;
        }

        public void AdicionaBem(Bem bem)
        {
            if (Bens == null)
                Bens = new List<Bem>();
            // Busco se o bem já estána lista
            List<Bem> retorno = Bens.Where(c => c.pk == bem.pk).ToList<Bem>();
            // Se não encontra nada, saí
            if (retorno.Count > 0)
                return;
            // Senão, registra
            Bens.Add(bem);
        }
    }
}
