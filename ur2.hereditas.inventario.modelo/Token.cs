﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class Token
    {
        private string _chave;
        public string chave { get { return _chave; } }

        public string name { get { return "Authorization"; } }
        public string value { get { return " Token " + chave; } } 
        public bool Valido { get { return chave.Length > 0; } }

        public Token(string chave)
        { _chave = chave.Trim(); }

        public override string ToString()
        {
            return "Authorization: Token " + chave;
        }
    }
}
