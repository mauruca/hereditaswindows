﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ur2.Hereditas.Inventario.Modelo
{
    public class LicencaDataTransport
    {
        public string serial { get; set; }
        public string chave { get; set; }

        public LicencaDataTransport(string s, string c)
        {
            serial = s;
            chave = c;
        }

        public string Serializar()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }
    }
}
