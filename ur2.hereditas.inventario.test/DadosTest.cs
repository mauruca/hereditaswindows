﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ur2.Hereditas.Inventario;
using Ur2.Hereditas.Inventario.Modelo;

namespace Ur2.Hereditas.Inventario.Test
{
    [TestClass]
    public class DadosTest
    {
        #region Inventario
        [TestMethod]
        public void TestInventarioIniciaNulo()
        {
            Dados d = new Dados();
            Assert.IsNull(d.inventarioatual);
        }
        [TestMethod]
        public void TestInventarioNuloNaoValido()
        {
            Dados d = new Dados();
            Assert.AreEqual<bool>(false, d.ExisteInventarioValido);
        }
        [TestMethod]
        public void TestInventarioNaoNuloNaoValido()
        {
            Dados d = new Dados();
            d.IniciaInventario();
            Assert.AreEqual<bool>(false, d.ExisteInventarioValido);
        }
        [TestMethod]
        public void TestInventarioIniciado()
        {
            Dados d = new Dados();
            d.IniciaInventario();
            Assert.IsInstanceOfType(d.inventarioatual,typeof(InventarioBem));
        }
        [TestMethod]
        public void TestInventarioNaoPodeIniciarInventario()
        {
            Dados d = new Dados();
            Assert.AreEqual<bool>(false, d.TemDadosParaIniciarInventario);
        }
        [TestMethod]
        public void TestInventarioPodeIniciarInventario()
        {
            Dados d = new Dados();
            d.IniciaInventario();
            d.Bens.Add(new Bem("a"));
            d.Locais.Add(new Local("1"));
            d.Pessoas.Add(new Pessoa("1"));
            Assert.AreEqual<bool>(true, d.TemDadosParaIniciarInventario);
        }
        [TestMethod]
        public void TestInventarioNaoExisteInventarioValido()
        {
            Dados d = new Dados();
            Assert.AreEqual<bool>(false, d.ExisteInventarioValido);
        }
        [TestMethod]
        public void TestInventarioExisteInventarioValido()
        {
            Dados d = new Dados();
            d.IniciaInventario();
            d.inventarioatual.Pessoa = new Pessoa("1");
            d.inventarioatual.Local = new Local("1");
            d.Bens.Add(new Bem("1"));
            Assert.AreEqual<bool>(false, d.ExisteInventarioValido);
        }
        [TestMethod]
        public void TestInventarioNaoTemInventarios()
        {
            Dados d = new Dados();
            Assert.AreEqual<bool>(false, d.TemInventarios);
        }
        [TestMethod]
        public void TestInventarioTemInventarios()
        {
            Dados d = new Dados();
            d.IniciaInventario();
            d.inventarioatual.Pessoa = new Pessoa("1");
            d.inventarioatual.Local = new Local("1");
            d.Bens.Add(new Bem("1"));
            d.Inventarios.Add(d.inventarioatual);
            Assert.AreEqual<bool>(false, d.ExisteInventarioValido);
        }
        #endregion Inventario
    }
}
