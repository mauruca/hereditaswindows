﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Ur2.REST.ClientProxy;
using Ur2.Hereditas.Inventario.Modelo;
using System.Configuration;

namespace Ur2.Hereditas.Inventario
{
    public class Dados
    {
        public const string USUARIONAOENCONTRATO = "Usuário não encontrado.";
        public const string LOCALNAOENCONTRATO = "Local não encontrado.";
        private const string PORTADOT900 = "Port=";
        
        public Token TokenUsuario { get; set; }
        public bool ExisteTokenAutenticacao{ 
            get{
                if (TokenUsuario == null || !TokenUsuario.Valido)
                {
                    return false;
                }
                return true;
            }
        }
        public Pessoa UsuarioAutenticado { get; set; }
        public bool ExisteUsuarioAutenticado {
            get
            {
                if (UsuarioAutenticado == null)
                {
                    return false;
                }
                if (!UsuarioAutenticado.Valido)
                {
                    // Tento encontrar a pessoa que autenticou
                    Pessoa aux = BuscaPessoaLogin(UsuarioAutenticado.login);
                    if (aux == null || !aux.Valido)
                    {
                        return false;
                    }
                    UsuarioAutenticado = aux;
                }
                return true;
            }
        }

        private const string RFIDcomm = "RFIDcomm.cfg";
        public string portaDOT900 { get; set; }

        private List<Bem> _associacaoBemRFID;
        private List<Local> _associacaoLocalRFID;
        private List<Pessoa> _associacaoInvRFID;

        public Dados()
        {
            _associacaoBemRFID = new List<Bem>();
            _associacaoLocalRFID = new List<Local>();
            _associacaoInvRFID = new List<Pessoa>();
            Inventarios = new List<InventarioBem>();
        }

        #region ListaDadosBens
        private List<Bem> _bens = new List<Bem>();
        public List<Bem> Bens { get { return _bens; } set { _bens = value; } }
        public bool TemDadosBens { get { return _bens.Count > 0; } }
        public Bem BuscaBem(string RFID)
        {
            List<Bem> retorno = Bens.Where(c => c.rfid == RFID).ToList<Bem>();
            if (retorno.Count == 0)
                return null;
            return retorno[0];
        }
        #endregion ListaDadosBens

        #region ListaDadosLocais
        private List<Local> _locais = new List<Local>();
        public List<Local> Locais { get { return _locais; } set { _locais = value; } }
        public bool TemDadosLocais { get { return _locais.Count > 0; } }
        public Local BuscaLocal(string RFID)
        {
            List<Local> retorno = Locais.Where(c => c.rfid == RFID).ToList<Local>();
            if (retorno.Count == 0)
                return null;
            return retorno[0];
        }
        #endregion ListaDadosLocais

        #region ListaDadosPessoas
        private List<Pessoa> _pessoas = new List<Pessoa>();
        public List<Pessoa> Pessoas { get { return _pessoas; } set { _pessoas = value; } }
        public bool TemDadosPessoas { get { return _pessoas.Count > 0; } }
        public Pessoa BuscaPessoa(string RFID)
        {
            List<Pessoa> retorno = Pessoas.Where(c => c.rfid == RFID).ToList<Pessoa>();
            if (retorno.Count == 0)
                return null;
            return retorno[0];
        }
        public Pessoa BuscaPessoaLogin(string login)
        {
            List<Pessoa> retorno = Pessoas.Where(c => c.login == login).ToList<Pessoa>();
            if (retorno.Count == 0)
                return null;
            return retorno[0];
        }
        #endregion ListaDadosPessoas

        public bool ExisteEntidadeComEsteTagRFID(string tagRFID)
        {
            // valida assertivas de entrada
            if (tagRFID.Trim().Length == 0)
            {
                throw new ArgumentOutOfRangeException("Tag RFID não pode ser nula.");
            }
            if (BuscaBem(tagRFID) != null)
                return true;
            if (BuscaLocal(tagRFID) != null)
                return true;
            if (BuscaPessoa(tagRFID) != null)
                return true;
            return false;
        }

        #region AssociacaoBemTagRFID
        public List<Bem> BensSemTagRFID
        {
            get
            {
                return Bens.FindAll(x => x.rfid.Length == 0);
            }
        }
        public List<Bem> AssociacaoBemRFID { get { return _associacaoBemRFID; } }
        public bool TemDadosAssociacaoBens { get { return AssociacaoBemRFID.Count > 0; } }
        public void AssociaTagBem(object bem, string tagRFID)
        {
            // valida assertivas
            if (bem == null)
                throw new ArgumentNullException("Objeto bem não pode ser nulo.");
            if (!(bem is Bem))
                throw new ArgumentException("Objeto deve ser um Bem");
            if (tagRFID.Trim().Length == 0)
                throw new ArgumentException("Tag não pode ser vazia.");

            ((Bem)bem).rfid = tagRFID;
            Bem aux = new Bem();
            aux.pk = ((Bem)bem).pk;
            aux.descricao = ((Bem)bem).descricao;
            aux.rfid = ((Bem)bem).rfid;
            AssociacaoBemRFID.Add(aux);
        }
        public string ObtemRFIDItemBemSelecionado(object bem)
        {
            if (bem == null)
                return string.Empty;
            if (!(bem is Bem))
                throw new ArgumentException("Objeto deve ser um bem");

            Bem aux = (Bem)bem;
            return aux.rfid;
        }
        public bool RemoveRFIDItemBemSelecionado(object bem)
        {
            if (bem == null || AssociacaoBemRFID == null)
                return false;
            if (!(bem is Bem))
                throw new ArgumentException("Objeto deve ser um bem");
            ((Bem)bem).rfid = string.Empty;
            List<Bem> retorno = AssociacaoBemRFID.Where(c => c.rfid == ((Bem)bem).rfid).ToList<Bem>();
            foreach (Bem item in retorno)
            {
                AssociacaoBemRFID.Remove(item);
            }
            return true;
        }
        private void LimpaAssociaTagBem()
        {
            _associacaoBemRFID = new List<Bem>();
        }
        #endregion AssociacaoBemTagRFID

        #region AssociacaoLocalTagRFID
        public List<Local> LocaisSemTagRFID
        {
            get
            {
                return Locais.FindAll(x => x.rfid.Length == 0);
            }
        }
        public List<Local> AssociacaoLocalRFID { get { return _associacaoLocalRFID; } }
        public bool TemDadosAssociacaoLocais { get { return AssociacaoLocalRFID.Count > 0; } }
        public void AssociaTagLocal(object local, string tagRFID)
        {
            // valida assertivas
            if (local == null)
                throw new ArgumentNullException("Objeto local não pode ser nulo.");
            if (!(local is Local))
                throw new ArgumentException("Objeto deve ser um local");
            if (tagRFID.Trim().Length == 0)
                throw new ArgumentException("Tag não pode ser vazia.");

            ((Local)local).rfid = tagRFID;
            Local aux = new Local();
            aux.pk = ((Local)local).pk;
            aux.rfid = ((Local)local).rfid;
            aux.nome = ((Local)local).nome;
            AssociacaoLocalRFID.Add(aux);
        }
        public string ObtemRFIDItemLocalSelecionado(object local)
        {
            if (local == null)
                return string.Empty;
            if (!(local is Local))
                throw new ArgumentException("Objeto deve ser um local");

            Local aux = (Local)local;
            return aux.rfid;
        }
        public bool RemoveRFIDItemLocalSelecionado(object local)
        {
            if (local == null || AssociacaoLocalRFID == null)
                return false;
            if (!(local is Local))
                throw new ArgumentException("Objeto deve ser um local");
            List<Local> retorno = AssociacaoLocalRFID.Where(c => c.rfid == ((Local)local).rfid).ToList<Local>();
            ((Local)local).rfid = string.Empty;
            foreach (Local item in retorno)
            {
                AssociacaoLocalRFID.Remove(item);
            }
            return true;
        }
        private void LimpaAssociacaoLocalRFID()
        {
            _associacaoLocalRFID = new List<Local>();
        }
        #endregion AssociacaoLocalTagRFID

        #region AssociacaoInvTagRFID
        public List<Pessoa> InvsSemTagRFID
        {
            get
            {
                return Pessoas.FindAll(x => x.rfid.Length == 0);
            }
        }
        public List<Pessoa> AssociacaoInvRFID { get { return _associacaoInvRFID; } }
        public bool TemDadosAssociacaoInvs { get { return AssociacaoInvRFID.Count > 0; } }
        public void AssociaTagInv(object inv, string tagRFID)
        {
            // valida assertivas
            if (inv == null)
                throw new ArgumentNullException("Objeto inventariante não pode ser nulo.");
            if (!(inv is Pessoa))
                throw new ArgumentException("Objeto deve ser um inventariante");
            if (tagRFID.Trim().Length == 0)
                throw new ArgumentException("Tag não pode ser vazia.");

            ((Pessoa)inv).rfid = tagRFID;
            Pessoa aux = new Pessoa();
            aux.pk = ((Pessoa)inv).pk;
            aux.user = ((Pessoa)inv).user;
            aux.rfid = ((Pessoa)inv).rfid;
            aux.nomecompleto = ((Pessoa)inv).nomecompleto;
            aux.login = ((Pessoa)inv).login;
            AssociacaoInvRFID.Add(aux);
        }
        public string ObtemRFIDItemInvSelecionado(object inv)
        {
            if (inv == null)
                return string.Empty;
            if (!(inv is Pessoa))
                throw new ArgumentException("Objeto deve ser um inventariante");

            Pessoa aux = (Pessoa)inv;
            return aux.rfid;
        }
        public bool RemoveRFIDItemInvSelecionado(object inv)
        {
            if (inv == null || AssociacaoInvRFID== null)
                return false;
            if (!(inv is Pessoa))
                throw new ArgumentException("Objeto deve ser um inventariante");
            List<Pessoa> retorno = AssociacaoInvRFID.Where(c => c.rfid == ((Pessoa)inv).rfid).ToList<Pessoa>();
            ((Pessoa)inv).rfid = string.Empty;
            foreach (Pessoa item in retorno)
            {
                AssociacaoInvRFID.Remove(item);
            }
            return true;
        }
        private void LimpaAssociacaoInvRFID()
        {
            _associacaoInvRFID = new List<Pessoa>();
        }
        #endregion AssociacaoInvTagRFID

        #region Inventario
        public InventarioBem inventarioatual = null;
        public List<InventarioBem> Inventarios { get; set; }
        public bool TemDadosParaIniciarInventario { get { return TemDadosBens && TemDadosLocais && TemDadosPessoas; } }
        public bool ExisteInventarioValido { get { return inventarioatual != null && inventarioatual.Valido; } }
        public bool TemInventarios { get { return Inventarios != null && Inventarios.Count > 0; } }
        public void IniciaInventario()
        {
            inventarioatual = new InventarioBem();
        }
        public void RegistraInventario()
        {
            if (inventarioatual.Valido)
                Inventarios.Add(inventarioatual);
            inventarioatual = null;
        }
        private void LimpaInventario()
        {
            Inventarios.Clear();
        }
        #endregion Inventario

        #region DadosNuvem
        public bool BaixarDados()
        {
            // valida assertivas de entrada
            if (!ExisteTokenAutenticacao)
                return false;
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");
                client.Method = HttpVerb.GET;
                Bem.Deserializador(client.MakeRequest("/api/bem/",TokenUsuario.name,TokenUsuario.value), ref _bens);
                Local.Deserializador(client.MakeRequest("/api/local/", TokenUsuario.name, TokenUsuario.value), ref _locais);
                Pessoa.Deserializador(client.MakeRequest("/api/pessoa/", TokenUsuario.name, TokenUsuario.value), ref _pessoas);
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool EnviarDadosNuvem()
        {
            if (!EnviarDadosNuvemAssociacaoBemRFID())
                return false;
            LimpaAssociaTagBem();
            if (!EnviarDadosNuvemAssociacaoLocalRFID())
                return false;
            LimpaAssociacaoLocalRFID();
            if (!EnviarDadosNuvemAssociacaoInvRFID())
                return false;
            LimpaAssociacaoInvRFID();
            if (!EnviarDadosNuvemInventario())
                return false;
            LimpaInventario();
            return true;
        }
        public bool EnviarDadosNuvemAssociacaoBemRFID()
        {
            // valida assertivas de entrada
            if (!ExisteTokenAutenticacao)
                return false;
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");

                foreach (Bem item in AssociacaoBemRFID)
                {
                    // criar idt do bem
                    BemUpdateDataTransport bdt = new BemUpdateDataTransport(item.rfid);
                    client.Method = HttpVerb.PUT;
                    client.PostData = bdt.Serializar();
                    if (item.pk.Length == 0)
                        continue;
                    client.MakeRequest("/api/bem/"+item.pk+"/", TokenUsuario.name, TokenUsuario.value);
                }
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool EnviarDadosNuvemAssociacaoLocalRFID()
        {
            // valida assertivas de entrada
            if (!ExisteTokenAutenticacao)
                return false;
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");

                foreach (Local item in AssociacaoLocalRFID)
                {
                    // criar idt do bem
                    LocalUpdateDataTransport ldt = new LocalUpdateDataTransport(item.rfid);
                    client.Method = HttpVerb.PUT;
                    client.PostData = ldt.Serializar();
                    if (item.pk.Length == 0)
                        continue;
                    client.MakeRequest("/api/local/" + item.pk + "/", TokenUsuario.name, TokenUsuario.value);
                }
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool EnviarDadosNuvemAssociacaoInvRFID()
        {
            // valida assertivas de entrada
            if (!ExisteTokenAutenticacao)
                return false;
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");

                foreach (Pessoa item in AssociacaoInvRFID)
                {
                    // criar idt do bem
                    InventarianteUpdateDataTransport bdt = new InventarianteUpdateDataTransport(item.rfid);
                    client.Method = HttpVerb.PUT;
                    client.PostData = bdt.Serializar();
                    if (item.pk.Length == 0)
                        continue;
                    client.MakeRequest("/api/pessoa/" + item.pk + "/", TokenUsuario.name, TokenUsuario.value);
                }
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool EnviarDadosNuvemInventario()
        {
            // valida assertivas de entrada
            if (!ExisteTokenAutenticacao)
                return false;
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");

                foreach (InventarioBem item in Inventarios)
                {
                    foreach (Bem it in item.Bens)
                    {
                        InventarioDataTransport idt = new InventarioDataTransport(item.Data.ToString("yyyy-MM-ddTHH:mm:ss"), it.pk, item.Local.pk, item.Pessoa.user);
                        client.Method = HttpVerb.POST;
                        client.PostData = idt.Serializar();
                        client.MakeRequest("/api/inventario/", TokenUsuario.name, TokenUsuario.value, System.Net.HttpStatusCode.Created);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public void DesconectaUsuario()
        {
            TokenUsuario = null;
            UsuarioAutenticado = null;
        }
        #endregion DadosNuvem

        #region DadosLocal
        public bool SalvaDadosDisco()
        {
            try
            {
                XMLSerializerList.Save<Local>(Locais);
                XMLSerializerList.Save<Bem>(Bens);
                XMLSerializerList.Save<Pessoa>(Pessoas);
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool SalvaInventarioDisco()
        {
            try
            {
                XMLSerializerList.Save<InventarioBem>(Inventarios);
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool CarregaDadosDisco()
        {
            try
            {
                Locais = XMLSerializerList.Load<Local>();
                Pessoas = XMLSerializerList.Load<Pessoa>();
                Bens = XMLSerializerList.Load<Bem>();
                Inventarios = XMLSerializerList.Load<InventarioBem>();
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        public bool ExcluiDadosInventario()
        {
            try
            {
                XMLSerializerList.Delete<InventarioBem>();
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
        #endregion DadosLocal

        #region Config
        public bool CarregaPortaDOT900()
        {
            try
            {
                portaDOT900 = string.Empty;
                using (StreamReader sr = new StreamReader(RFIDcomm))
                {
                    String line = sr.ReadToEnd();
                    if (line.Trim().Length > 0 && line.Contains(PORTADOT900))
                        portaDOT900 = line.Split('=')[1];
                }
                return true;
            }
            catch (Exception ex)
            {
                portaDOT900 = string.Empty;
                Erro.Registra(ex);
                return false;
            }
        }
        public string MensagemErroConfigDOT900Ler {
            get { return "Não foi possível ler a configuração da porta de comunicação do DOT900. Verificar a configuração RFIDcomm existe e se contem o nome do arquivo. Verificar se o arquivo existe e contém o valor " + PORTADOT900 + "<valor>."; }
        }
        public string MensagemErroConfigDOT900Salvar
        {
            get { return "Não foi possível salvar a configuração da porta de comunicação do DOT900. Verificar a configuração RFIDcomm existe e se contem o nome do arquivo."; }
        }
        public string MensagemConfigDOT900Invalido
        {
            get { return "Porta de dispositivo não configurada. O valor deve ser maior que zero."; }
        }
        public string MensagemDispositivoNaoEncontrado
        {
            get { return "Dispositivo não encontrado para iniciar a leitura."; }
        }
        public bool AlteraPortaDOT900()
        {
            try
            {
                StreamWriter outputFile = new StreamWriter(RFIDcomm);
                Task task = Task.Factory.StartNew(() =>  outputFile.WriteAsync(PORTADOT900 + portaDOT900));
                Task.WaitAll(task);
                outputFile.Flush();
                outputFile.Dispose();
                task.Dispose();
                return true;
            }
            catch (Exception e)
            {
                CaixaMensagem m = new CaixaMensagem(MensagemErroConfigDOT900Salvar);
                m.ShowDialog();
                Erro.Registra(e);
                return false;
            }
        }
        #endregion Config
    }
}
