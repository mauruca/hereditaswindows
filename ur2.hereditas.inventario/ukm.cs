﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Security.Cryptography;
using System.Configuration;
using Ur2.REST.ClientProxy;
using Ur2.Hereditas.Inventario.Modelo;
using System.Reflection;

namespace Ur2.Hereditas.Inventario
{
    public class ukm
    {
        public string Serial {
            get {
                return ConfigurationManager.AppSettings.Get("serial");
            }
        }
        public string Chave {
            get {
                return ToHashMD5(Processador() + OS() + Disk() + Serial);
            }
        }
        public string Versao {
            get {
                var obj = Assembly.GetExecutingAssembly().GetName().Version;
                string v1 = string.Format("{0}.{1}", obj.Build, obj.Revision);
                string v2 = string.Format("{0}.{1}", obj.Major, obj.Minor);
                return v2;
            }
        }

        public bool ValidaLicenca()
        {
            Auth a = new Auth();
            try
            {
                if (a.ValidaLicenca(Serial, Chave))
                    return true;
                return false;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
            }
            finally {
                a = null;
            }
            return false;
        }
        private string Processador()
        {
            var cpu =
                new ManagementObjectSearcher("select * from Win32_Processor")
                .Get()
                .Cast<ManagementObject>()
                .First();

            return ToHashMD5(cpu["ProcessorId"].ToString() + cpu["Caption"].ToString() + cpu["Name"].ToString() + ((uint)cpu["NumberOfCores"]).ToString());
        }
        private string OS()
        {
            var os = new ManagementObjectSearcher("select * from Win32_OperatingSystem")
                    .Get().Cast<ManagementObject>().First();

            return ToHashMD5(os["SerialNumber"].ToString().Replace("-",""));
        }
        private string Disk()
        {
            var hd = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia")
                    .Get().Cast<ManagementObject>().First();

            return ToHashMD5(hd["SerialNumber"].ToString());
        }
        private string ToHashMD5(string input)
        {
            // valida assertivas de entrada
            if (input.Trim().Length == 0)
                return string.Empty;

            MD5 md5 = MD5.Create();
            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // valida assertivas de saida
            if (sBuilder.Length == 0)
                throw new ApplicationException("Processed hash is empty.");

            return sBuilder.ToString();
        }
    }
}
