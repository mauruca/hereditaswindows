﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace Ur2.Hereditas.Inventario
{
    public class XMLSerializerList
    {
        public static List<T> Load<T>(bool encrypt = true)
        {
            string filename = Environment.CurrentDirectory + "\\" + typeof(T).Name + FileExtensions(encrypt);
            if (!File.Exists(filename))
                return new List<T>();

            XmlSerializer leitor = new XmlSerializer(typeof(List<T>));
            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);

            if (!encrypt)
            {
                List<T> retorno = (List<T>)leitor.Deserialize(file);
                file.Close();
                return retorno;
            }
            MemoryStream ms = new MemoryStream();
            EncryptionManagement.Decrypt(file, ms);
            ms.Seek(0, SeekOrigin.Begin);
            List<T> retorno2 = (List<T>)leitor.Deserialize(ms);
            return retorno2;
        }

        public static void Save<T>(List<T> list, bool encrypt = true)
        {
            string filename = Environment.CurrentDirectory + "\\" + typeof(T).Name + FileExtensions(encrypt);

            XmlSerializer writer = new XmlSerializer(typeof(List<T>));
            FileStream file = new FileStream(filename, FileMode.Create, FileAccess.Write);
            if (!encrypt)
            {
                writer.Serialize(file, list);
                file.Close();
                return;
            }
            
            MemoryStream ms = new MemoryStream();
            writer.Serialize(ms, list);
            ms.Seek(0, SeekOrigin.Begin);
            EncryptionManagement.Encrypt(ms, file);
            file.Close();
        }

        public static void Delete<T>(bool encrypt = true)
        {
            string filename = Environment.CurrentDirectory + "\\" + typeof(T).Name + FileExtensions(encrypt);
            if (File.Exists(filename))
                File.Delete(filename);
        }

        private static string FileExtensions(bool encrypt)
        {
            if (encrypt)
                return ".data";
            return ".xml";
        }
    }

    public static class EncryptionManagement
    {
        private static SymmetricAlgorithm encryption;

        private static void Init()
        {
            ukm u = new ukm();
            string Mkey = u.Chave;
            encryption = new RijndaelManaged();
            var key = new Rfc2898DeriveBytes(u.Serial, Encoding.ASCII.GetBytes(Mkey));

            encryption.Key = key.GetBytes(encryption.KeySize / 8);
            encryption.IV = key.GetBytes(encryption.BlockSize / 8);
            encryption.Padding = PaddingMode.PKCS7;
        }

        public static void Encrypt(Stream inStream, Stream OutStream)
        {
            Init();
            var encryptor = encryption.CreateEncryptor();
            inStream.Position = 0;
            var encryptStream = new CryptoStream(OutStream, encryptor, CryptoStreamMode.Write);
            inStream.CopyTo(encryptStream);
            encryptStream.FlushFinalBlock();
        }

        public static void Decrypt(Stream inStream, Stream OutStream)
        {
            Init();
            var dencryptor = encryption.CreateDecryptor();
            inStream.Position = 0;
            var encryptStream = new CryptoStream(inStream, dencryptor, CryptoStreamMode.Read);
            encryptStream.CopyTo(OutStream);
            OutStream.Position = 0;
        }
    }
}
