﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.REST.ClientProxy;
using Ur2.Hereditas.Inventario.Modelo;
using System.Configuration;

namespace Ur2.Hereditas.Inventario
{
    public class Auth
    {
        private Token _token;

        public Token token { get { return _token; } }
        public bool ValidaUsuario(string usuario, string senha)
        {
            // validar assertivas de entrada
            if (usuario.Trim().Length == 0 || senha.Trim().Length == 0)
            {
                throw new ArgumentException("Usuário ou senha não podem ser vazios.");
            }
            if (Valida(usuario.Trim(), senha.Trim(), true))
            {
                return true;
            }
            return false;
        }
        public bool ValidaLicenca(string serial, string chave)
        {
            // validar assertivas de entrada
            if (serial.Trim().Length == 0 || chave.Trim().Length == 0)
            {
                throw new ArgumentException("Serial ou chave não podem ser vazios.");
            }
            if (Valida(serial, chave))
            {
                return true;
            }
            return false;
        }
        private bool Valida(string usuario, string senha, bool salvatoken = false)
        {
            // validar assertivas de entrada
            if (usuario.Trim().Length == 0 || senha.Trim().Length == 0)
            {
                return false;
            }
            try
            {
                RESTClientProxy client = new RESTClientProxy();
                client.EndPoint = ConfigurationManager.AppSettings.Get("apiURI");
                AuthDataTransport adt = new AuthDataTransport(usuario,senha);
                client.PostData = adt.Serializar();
                client.Method = HttpVerb.POST;
                string retorno = client.MakeRequest("/api-token-auth/");
                if(salvatoken)
                    _token = TokenDataTransport.Deserializador(retorno);
                return true;
            }
            catch (Exception e)
            {
                Erro.Registra(e);
                return false;
            }
        }
    }
}
