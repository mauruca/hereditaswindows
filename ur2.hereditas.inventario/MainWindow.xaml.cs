﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using System.Threading;

namespace Ur2.Hereditas.Inventario
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DeviceType tipoDispositivoUsado = DeviceType.DOTR900;
        private static IDevice dispositivoAtual;
        private Dados dados = new Dados();
        private System.Timers.Timer statusTimer = new System.Timers.Timer();
        private List<string> comports = new List<string>();
        private SemaphoreSlim semDispositivo = new SemaphoreSlim(1,1);

        public MainWindow()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception e)
            {
                Erro.Registra(e);
            }
        }

        #region Eventos
        private void Window_Closed(object sender, EventArgs e)
        {
            FechaApp();
        }
        private void SairBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            statusTimer.Stop();
            this.Close();
        }
        private void iniciarInventario_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IniciaInventario();
        }
        private void pararInventario_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ParaLeituraInventario(true);
        }
        private void LeitorOnBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DesligaDispositivo();
        }
        private void LeitorOffBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            LigaDispositivo();
        }
        private void TituloMoveRet_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MoveTela(sender, e);
        }
        private void MoveTela(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void baixarBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CarregaDadosServidor();
        }
        private void enviarBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EnviaDados();
        }
        private void SairSerialBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeLicenca();
        }
        private void LicencaBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraLicenca();
        }
        private void labelMachinaValor_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CopiaChave();
        }
        private void SairDadosServidorBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeSincronia();
        }
        private void FazerLoginBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AutenticarUsuario();
        }
        private void FazerLogoutBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DesconectarUsuario();
        }
        private void ServidorBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraSincronia();
        }
        private void InventarioBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraInventario();
        }
        private void SairInventarioBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FecharTelaInventario(true);
        }
        private void ConfigBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraConfiguracao();
        }
        private void SairConfigBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeConfiguracao();
        }
        private void labelSerialValor_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CopiaSerial();
        }
        private void AssocBemRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraAssociacaoBemRFID();
        }
        private void SairBemRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeAssociacaoBemRFID();
        }
        private void listBoxBemSemRFID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MudaTagAssociacaoBemRFIDSelecionada();
        }
        private void TagBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            LeTagBemRFID();
        }
        private void TagRemoveBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RemoveTagAssociacaoBemRFID();
        }
        private void PortaDOT900textBox_KeyUp(object sender, KeyEventArgs e)
        {
            SalvaConfiguracao();
        }
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                IniciaApp();
            }
            catch (Exception ex)
            {
                Erro.Registra(ex);
                MostraStatus("Erro ao inicializar o aplicativo." + ex.Message);
                Mouse.OverrideCursor = null;
            }
        }
        private void AssocInvRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraAssociacaoInvRFID();
        }
        private void SairInvRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeAssociacaoInvRFID();
        }
        private void InvTagBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            LeTagInvRFID();
        }
        private void InvRemoveTagBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RemoveTagAssociacaoInvRFID();
        }
        private void listBoxInvSemRFID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MudaTagAssociacaoInvRFIDSelecionada();
        }
        private void senhapasswordBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AutenticarUsuario();
            }
        }
        private void usertextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AutenticarUsuario();
            }
        }
        private void BtSerialLicenca_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeMostraThread(LicencaCanvas);
        }
        private void BtSairSerialLicenca_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeMostraThread(LicencaCanvas, true);
        }
        private void listBoxLocalSemRFID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MudaTagAssociacaoLocalRFIDSelecionada();
        }
        private void LocalTagBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            LeTagLocalRFID();
        }
        private void LocalTagRemoveBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RemoveTagAssociacaoLocalRFID();
        }
        private void SairLocalRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeAssociacaoLocalRFID();
        }
        private void AssocLocalRFIDBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            MostraAssociacaoLocalRFID();
        }
        private void LoginBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeMostraThread(Login, false);
            MostraTodosItensMenu(false);
            usertextBox.Focus();
        }
        private void SairLoginBt_MouseUp(object sender, MouseButtonEventArgs e)
        {
            EscondeMostraThread(Login,true);
            MostraTodosItensMenu();
        }
        #endregion Eventos

        #region Controle do Device
        private void DeviceCreate()
        {
            semDispositivo.Wait(1000);
            DeviceClose();
            dispositivoAtual = DeviceFactory.CreateDevice(tipoDispositivoUsado);
            if (!DispositivoExiste())
            {
                MostraStatus("Falha ao criar dispositivo.");
            }
            dispositivoAtual.DefineWindowHaldle();
            semDispositivo.Release();
        }
        private void DeviceOpen()
        {
            semDispositivo.Wait();
            try
            {
                DeviceClose();
                if (!dispositivoAtual.Open())
                {
                    MostraStatus("Dispositivo de leitura não encontrado.");
                    return;
                }
                AtivaDeviceTela();
            }
            catch (InvalidOperationException oe)
            {
                MostraStatus("Dispositivo ainda em uso. Tente novamente.");
                Erro.Registra(oe);
            }
            catch (Exception e)
            {
                MostraStatus("Erro ao tentar abrir a conexão: " + e.Message);
                Erro.Registra(e);
            }
            finally
            {
                semDispositivo.Release();
            }
        }
        private void DeviceClose()
        {
            if (DispositivoAtivo())
                dispositivoAtual.Close();
            DesativaDeviceTela();
        }
        private void CriaDispositivoThread()
        {
            // Ao encontrar tags para a leitura
            ParaBuscaTagAssociacaoBemRFID();

            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => CriaDispositivo());
                return;
            }
            CriaDispositivo();
        }
        private void CriaDispositivo()
        {
            dispositivoAtual = DeviceFactory.CreateDevice(tipoDispositivoUsado);
        }
        private bool IniciaLeitura(bool continuo = false, bool limpamemoria = true)
        {
            try
            {
                if (!DispositivoAtivo())
                    return false;
                dispositivoAtual.StartReading(continuo, limpamemoria);
                return true;
            }
            catch (Exception e)
            {
                MostraStatus("Erro ao iniciar leitura: " + e.Message);
                Erro.Registra(e);
                return false;
            }
        }
        private void ParaLeitura()
        {
            if (DispositivoAtivo())
                dispositivoAtual.StopReading();
        }
        private void AtivarDeviceWait(bool mostra = true)
        {
            EscondeMostraThread(LeitorWaitBt,!mostra);
        }
        private void AtivaDeviceTela()
        {
            EscondeMostraThread(LeitorOnBt);
            EscondeMostraThread(LeitorOffBt, true);
        }
        private void DesativaDeviceTela()
        {
            EscondeMostraThread(LeitorOnBt, true);
            EscondeMostraThread(LeitorOffBt);
        }
        private bool DispositivoExiste()
        {
            if (dispositivoAtual is IDevice)
                return true;
            return false;

        }
        private bool DispositivoAtivo()
        {
            if (!DispositivoExiste() || !dispositivoAtual.IsActive)
                return false;
            return true;
        }
        #endregion Controle do Device

        #region App
        private async void IniciaApp()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            MostraStatus("Iniciando o sistema");
            // inicia timer de status temporario
            Task configstatustask = Task.Factory.StartNew(() => ConfigStatusTimerThread());
            await configstatustask;
            await Task.Delay(1000);
            // Carregar dados disco
            MostraStatus("Dados do disco - Carregando...");
            Task<bool> carregadadostask = Task.Factory.StartNew(() => dados.CarregaDadosDisco());
            await carregadadostask;
            if (!carregadadostask.Result)
            {
                MostraStatus("Dados do disco - Não foi feita a carga.");
            }
            else {
                MostraStatus("Dados do disco - OK - Pessoas: " + dados.Pessoas.Count.ToString() + " Locais: " + dados.Locais.Count.ToString() + " Bens: " + dados.Bens.Count.ToString() + " Inventarios: " + dados.Inventarios.Count.ToString());
            }
            Mouse.OverrideCursor = null;
        }
        private void FechaApp()
        {
            semDispositivo.Wait();
            DeviceClose();
            semDispositivo.Release();
            Application.Current.Shutdown();
            Environment.Exit(0);
        }
        private void LigaDispositivo()
        {
            DeviceCreate();
            LigaDispositivoAsync();
        }
        private async void LigaDispositivoAsync()
        {
            List<Task> tasks = new List<Task>();
            Mouse.OverrideCursor = Cursors.Wait;
            Task AtivarDeviceWaittask = Task.Factory.StartNew(() => AtivarDeviceWait());
            tasks.Add(AtivarDeviceWaittask);
            MostraStatus("Procurando dispositivo...", 2);
            //DeviceOpen();
            Task deviceopentask = Task.Factory.StartNew(() => DeviceOpen());
            tasks.Add(deviceopentask);
            await deviceopentask;
            var final = Task.Factory.ContinueWhenAll(tasks.ToArray(), fim => AtivarDeviceWait(false));
            await final;
            Mouse.OverrideCursor = null;
        }
        private async void DesligaDispositivo()
        {
            List<Task> tasks = new List<Task>();
            Mouse.OverrideCursor = Cursors.Wait;
            Task AtivarDeviceWaittask = Task.Factory.StartNew(() => AtivarDeviceWait());
            tasks.Add(AtivarDeviceWaittask);
            semDispositivo.Wait();
            MostraStatus("Cancelando a leitura...", 1);
            var ParaLeituraTask = Task.Factory.StartNew(() => ParaLeitura());
            tasks.Add(ParaLeituraTask);
            await ParaLeituraTask;
            MostraStatus("Desconectando dispositivo...", 1);
            var deviceCloseTask = Task.Factory.StartNew(() => DeviceClose());
            tasks.Add(deviceCloseTask);
            await deviceCloseTask;
            semDispositivo.Release();
            var final = Task.Factory.ContinueWhenAll(tasks.ToArray(), fim => AtivarDeviceWait(false));
            await final;
            Mouse.OverrideCursor = null;
        }
        private void MostraTodosItensMenu(bool mostra = true)
        {
            EscondeMostraThread(InibeRect, mostra);
        }

        #region Inventario
        private void IniciaInventario()
        {
            // verifica assertivas de entrada
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return;
            }
            if (!dados.TemDadosParaIniciarInventario)
            {
                MostraStatus("Faltam dados para executar inventario. Verificar se foram cadastrados: bens, pessoas e locais.");
                return;
            }
            if (!ExisteInventarianteSelecionado())
            {
                MostraStatus("Selecionar um inventariante.");
                return;
            }
            // Limpa dados e ajusta elementos de tela
            LimparInventario();

            // Muda botão para cancelamento
            MostraBotaoCancelarInventario();
            EscondeBens();
            EscondeLocal();

            // Inicia memoria de inventario
            dados.IniciaInventario();
            // Registra o inventariante selecionado
            dados.inventarioatual.AdicionaPessoa(comboBoxInventariante.SelectedItem);

            // Inicia busca de local
            MostraLocal();
            IniciaBuscaLocal();
            return;
        }
        private void ParaLeituraInventario(bool registraInventario = false)
        {
            semDispositivo.Wait();
            ParaBuscaLocal();
            ParaBuscaBens();
            if(registraInventario)
                RegistraInventarioAtual();
            MostraBotaoIniciarInventario();
            semDispositivo.Release();
        }
        private void MostraInventario()
        {
            MostraTodosItensMenu(false);
            EscondeInventariante();
            Mostranventariante();
            EscondeLocal();
            EscondeBens();
            EscondeMostraThread(TelaInventario);
        }
        private async void FecharTelaInventario(bool salvadados = false)
        {
            ParaLeituraInventario();
            if (salvadados && dados.TemInventarios)
            {
                Task<bool> salvaInventarioDiscoTask = Task.Factory.StartNew(() => dados.SalvaInventarioDisco());
                if (!await salvaInventarioDiscoTask)
                {
                    MostraStatus("Dados dos inventários não foram salvos.");
                }
                else {
                    MostraStatus("Dados dos inventários salvos.");
                }
            }
            EscondeMostraThread(TelaInventario, true);
            MostraTodosItensMenu();
        }
        private void MostraBotaoIniciarInventario()
        {
            EscondeMostraThread(cancelarBt, true);
            EscondeMostraThread(iniciarBt);
        }
        private void MostraBotaoCancelarInventario()
        {
            EscondeMostraThread(cancelarBt);
            EscondeMostraThread(iniciarBt, true);
        }
        private void LimparInventario()
        {
            LimparInventariante();
            LimparLocal();
            LimparBens();
        }
        #region Inventariante
        private bool IniciaBuscaInventariante()
        {
            semDispositivo.Wait();
            ParaBuscaInventariante();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaInventariante);
            semDispositivo.Release();
            if (!IniciaLeitura(true))
                return false;
            return true;
        }
        private void ParaBuscaInventariante()
        {
            ParaLeitura();
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaInventariante);
        }
        private void AtualizaInventariante()
        {
            ParaBuscaInventariante();
            LimparInventariante();
            bool buscaMemoria = false;
            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                var aux = dados.BuscaPessoa(item.Id);
                if (aux != null)
                {
                    dados.inventarioatual.Pessoa = aux;
                    buscaMemoria = true;
                }
            }
            if (!buscaMemoria)
            {
                AlteraInventarianteBox(Dados.USUARIONAOENCONTRATO);
                IniciaBuscaInventariante();
                return;
            }
            AlteraInventarianteBox(dados.inventarioatual.Pessoa.nomecompleto);
            // Inicia busca de local
            MostraLocal();
            IniciaBuscaLocal();
        }
        private void AlteraInventarianteBox(string texto)
        {
            // Verifica se pode 
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraInventarianteBox(texto));
                return;
            }
            inventarianteConteudo.Content = texto;
            inventarianteConteudo.UpdateLayout();
        }
        private void Mostranventariante()
        {
            CarregaInventariantes();
            EscondeMostraThread(inventarianteLabel);
            EscondeMostraThread(inventarianteRet);
            //EscondeMostraThread(inventarianteConteudo);
        }
        private void EscondeInventariante()
        {
            //EscondeMostraThread(inventarianteLabel,true);
            //EscondeMostraThread(inventarianteRet,true);
            EscondeMostraThread(inventarianteConteudo,true);
        }
        private void LimparInventariante()
        {
            AlteraInventarianteBox(string.Empty);
        }
        private void CarregaInventariantes()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => CarregaInventariantes());
                return;
            }
            comboBoxInventariante.ItemsSource = dados.Pessoas;
            comboBoxInventariante.UpdateLayout();
        }
        private bool ExisteInventarianteSelecionado()
        {
            if (!Dispatcher.CheckAccess())
            {
                return Dispatcher.Invoke(() => ExisteInventarianteSelecionado());
            }
            return comboBoxInventariante.SelectedIndex != -1;
        }
        #endregion Inventariante
        #region Local
        private void IniciaBuscaLocal()
        {
            // verifica assertivas de entrada
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return;
            }
            ParaBuscaLocal();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaLocalInventario);
            IniciaLeitura(true);
        }
        private void ParaBuscaLocal()
        {
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaLocalInventario);
            ParaLeitura();
        }
        private void AtualizaLocalInventario()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AtualizaLocalInventario());
                return;
            }
            ParaBuscaLocal();
            LimparLocal();
            bool buscaBem = false;
            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                var aux = dados.BuscaLocal(item.Id);
                if (aux != null)
                {
                    dados.inventarioatual.Local = aux;
                    buscaBem = true;
                }
            }
            if (!buscaBem)
            {
                AlteraLocalBox(Dados.LOCALNAOENCONTRATO);
                IniciaBuscaLocal();
                return;
            }
            AlteraLocalBox(dados.inventarioatual.Local.nome);
            // Inicia o inventário
            MostraBens();
            IniciaBuscaBens();
        }
        private void AlteraLocalBox(string texto)
        {
            localConteudo.Content = texto;
            localConteudo.UpdateLayout();
        }
        private void MostraLocal()
        {
            EscondeMostraThread(labelLocal);
            EscondeMostraThread(localConteudo);
            EscondeMostraThread(localRet);
        }
        private void EscondeLocal()
        {
            EscondeMostraThread(labelLocal,true);
            EscondeMostraThread(localConteudo,true);
            EscondeMostraThread(localRet,true);
        }
        private void LimparLocal()
        {
            AlteraLocalBox(string.Empty);
        }
        #endregion Local
        #region Bens
        private void IniciaBuscaBens()
        {
            // verifica assertivas de entrada
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return;
            }
            ParaBuscaBens();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaInventarioAtual);
            IniciaLeitura(true);
        }
        private void ParaBuscaBens()
        {
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaInventarioAtual);
            ParaLeitura();
        }
        private void AtualizaInventarioAtual()
        {
            ParaBuscaBens();
            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                // Vejo se o bem está na lista de bens pela tag encontrada
                var aux = dados.BuscaBem(item.Id);
                if (aux != null)
                {
                    // Registro o bem no inventario atual
                    dados.inventarioatual.AdicionaBem(aux);
                    // Mostro o nome do bem na tela
                    AlteraBens(aux.descricao);
                }
            }
            // Continuo a busca de bens
            IniciaBuscaBens();
        }
        private void RegistraInventarioAtual()
        {
            if (!dados.ExisteInventarioValido)
            {
                    MostraStatus("Inventario não armazenado por estar incompleto!");
                    return;
            }
            dados.RegistraInventario();
        }
        private void AlteraBens(string texto, bool concatena = true)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraBens(texto,concatena));
                return;
            }

            if (texto.Length == 0)
                return;

            if (textBens.Text.Contains(texto))
                return;

            if (!concatena)
            {
                textBens.Text = texto;
                textBens.UpdateLayout();
                return;
            }

            if (textBens.Text.Length > 0)
                textBens.Text += '\n';
            textBens.Text += texto;
            textBens.UpdateLayout();
        }
        private void MostraBens()
        {
            EscondeMostraThread(labelBens);
            EscondeMostraThread(textBens);
            EscondeMostraThread(bensRet);
        }
        private void EscondeBens()
        {
            EscondeMostraThread(labelBens, true);
            EscondeMostraThread(textBens, true);
            EscondeMostraThread(bensRet, true);
        }
        private void LimparBens()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => LimparBens());
                return;
            }
            textBens.Text = string.Empty;
            textBens.UpdateLayout();
        }
        #endregion Bens
        #endregion Inventario

        #region AssociacaoBemRFID
        private void MostraAssociacaoBemRFID()
        {
            CarregaBemSemRFID();
            EscondeMostraThread(AssociacaoBemRFID);
            MostraTodosItensMenu(false);
        }
        private void EscondeAssociacaoBemRFID()
        {
            EscondeMostraThread(AssociacaoBemRFID,true);
            listBoxBemSemRFID.SelectedIndex = -1;
            MostraTodosItensMenu();
        }
        private void CarregaBemSemRFID()
        {
            // Busco todos os bens sem RFID
            listBoxBemSemRFID.ItemsSource = dados.BensSemTagRFID;
            listBoxBemSemRFID.SelectedIndex = -1;
        }
        private object ItemBemSelecionado()
        {
            return listBoxBemSemRFID.SelectedItem;
        }
        private bool TemBemSemRFIDSelecionado()
        {
            return (listBoxBemSemRFID.SelectedItem is Modelo.Bem);
        }
        private void LeTagBemRFID()
        {
            if (!TemBemSemRFIDSelecionado())
            {
                MostraStatus("Selecione um bem para aplicar o tag RFID.");
                return;
            }
            IniciaBuscaTagAssociacaoBemRFID();
        }
        private bool IniciaBuscaTagAssociacaoBemRFID()
        {
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return false;
            }
            ParaBuscaTagAssociacaoBemRFID();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagAssociacaoBemRFID);
            if (!IniciaLeitura(true))
                return false;
            return true;
        }
        private void ParaBuscaTagAssociacaoBemRFID()
        {
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaTagAssociacaoBemRFID);
            ParaLeitura();
        }
        private void AtualizaTagAssociacaoBemRFID()
        {
            // Avalia a lista de tags encontradas
            foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
            {
                try
                {
                    // Ao encontrar tags para a leitura
                    ParaBuscaTagAssociacaoBemRFID();

                    if (!Dispatcher.CheckAccess())
                    {
                        Dispatcher.Invoke(() => AtualizaTagAssociacaoBemRFID());
                        return;
                    }
                    // Se a tag não está associada a um bem, local ou pessoa que está na memória do sistema
                    // Mesmo assim, ainda pode dar erro ao integrar com o servidor, se inserida a mesma tag em outro objeto
                    if (dados.ExisteEntidadeComEsteTagRFID(item.Id))
                    {
                        MostraStatus("Tag RFID encontrada já está vinculada.");
                        continue;
                    }
                    dados.AssociaTagBem(ItemBemSelecionado(), item.Id);
                    AlteraTagAssociacaoBemRFIDBox(item.Id);
                    return;
                }
                catch (Exception e)
                {
                    MostraStatus("Erro ao tentar ler a Tag RFID - " + e.Message);
                    Erro.Registra(e);
                }
            }
        }
        private void AlteraTagAssociacaoBemRFIDBox(string texto)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraTagAssociacaoBemRFIDBox(texto));
                return;
            }
            texto = texto.Trim();
            labelTagBemRFIDValue.Content = texto;
            labelTagBemRFIDValue.UpdateLayout();
            if (texto.Length > 0)
                EscondeMostra(TagRemoveBt);
        }
        private void MudaTagAssociacaoBemRFIDSelecionada()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => MudaTagAssociacaoBemRFIDSelecionada());
                return;
            }
            labelTagBemRFIDValue.Content = dados.ObtemRFIDItemBemSelecionado(ItemBemSelecionado());
            string aux = dados.ObtemRFIDItemBemSelecionado(ItemBemSelecionado()).Trim();
            labelTagBemRFIDValue.Content = aux;
            if (aux.Length == 0)
            {
                EscondeMostra(TagRemoveBt, true);
                return;
            }
            EscondeMostra(TagRemoveBt);
        }
        private void RemoveTagAssociacaoBemRFID()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => RemoveTagAssociacaoBemRFID());
                return;
            }
            if (!TemBemSemRFIDSelecionado())
            {
                MostraStatus("Selecione um local para remover o tag RFID.");
                return;
            }
            if (dados.RemoveRFIDItemBemSelecionado(ItemBemSelecionado()))
            {
                EscondeMostra(TagRemoveBt, true);
                labelTagBemRFIDValue.Content = string.Empty;
            }
        }
        #endregion AssociacaoBemRFID

        #region AssociacaoLocalRFID
        private void CarregaLocalSemRFID()
        {
            // Busco todos os Localentariantes sem RFID
            listBoxLocalSemRFID.ItemsSource = dados.LocaisSemTagRFID;
            listBoxLocalSemRFID.SelectedIndex = -1;
        }
        private void MostraAssociacaoLocalRFID()
        {
            CarregaLocalSemRFID();
            EscondeMostraThread(AssociacaoLocalRFID);
            MostraTodosItensMenu(false);
        }
        private void EscondeAssociacaoLocalRFID()
        {
            EscondeMostraThread(AssociacaoLocalRFID, true);
            MostraTodosItensMenu();
            listBoxLocalSemRFID.SelectedIndex = -1;
        }
        private object ItemLocalentarianteSelecionado()
        {
            return listBoxLocalSemRFID.SelectedItem;
        }
        private bool TemLocalSemRFIDSelecionado()
        {
            return (listBoxLocalSemRFID.SelectedItem is Modelo.Local);
        }
        private void LeTagLocalRFID()
        {
            if (!TemLocalSemRFIDSelecionado())
            {
                MostraStatus("Selecione um Localentariante para aplicar o tag RFID.");
                return;
            }
            IniciaBuscaTagAssociacaoLocalRFID();
        }
        private bool IniciaBuscaTagAssociacaoLocalRFID()
        {
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return false;
            }
            ParaBuscaTagAssociacaoLocalRFID();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagAssociacaoLocalRFID);
            if (!IniciaLeitura(true))
                return false;
            return true;
        }
        private void ParaBuscaTagAssociacaoLocalRFID()
        {
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaTagAssociacaoLocalRFID);
            ParaLeitura();
        }
        private void AtualizaTagAssociacaoLocalRFID()
        {
            try
            {
                // Ao encontrar tags para a leitura
                ParaBuscaTagAssociacaoLocalRFID();

                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke(() => AtualizaTagAssociacaoLocalRFID());
                    return;
                }
                // Avalia a lista de tags encontradas
                foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
                {
                    // Se a tag não está associada a um bem, local ou pessoa que está na memória do sistema
                    // Mesmo assim, ainda pode dar erro ao integrar com o servidor, se inserida a mesma tag em outro objeto
                    if (dados.ExisteEntidadeComEsteTagRFID(item.Id))
                    {
                        MostraStatus("Tag RFID encontrada já está vinculada.");
                        continue;
                    }
                    dados.AssociaTagLocal(ItemLocalentarianteSelecionado(), item.Id);
                    AlteraTagAssociacaoLocalRFIDBox(item.Id);
                    return;
                }
            }
            catch (Exception e)
            {
                MostraStatus("Erro ao tentar ler a Tag RFID - " + e.Message);
                Erro.Registra(e);
            }
        }
        private void AlteraTagAssociacaoLocalRFIDBox(string texto)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraTagAssociacaoLocalRFIDBox(texto));
                return;
            }
            texto = texto.Trim();
            labelTagLocalRFIDValue.Content = texto;
            labelTagLocalRFIDValue.UpdateLayout();
            if (texto.Length > 0)
                EscondeMostra(LocalTagRemoveBt);
        }
        private void MudaTagAssociacaoLocalRFIDSelecionada()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => MudaTagAssociacaoLocalRFIDSelecionada());
                return;
            }
            string aux = dados.ObtemRFIDItemLocalSelecionado(ItemLocalentarianteSelecionado()).Trim();
            labelTagLocalRFIDValue.Content = aux;
            if (aux.Length == 0)
            {
                EscondeMostra(LocalTagRemoveBt, true);
                return;
            }
            EscondeMostra(LocalTagRemoveBt);
        }
        private void RemoveTagAssociacaoLocalRFID()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => RemoveTagAssociacaoLocalRFID());
                return;
            }
            if (!TemLocalSemRFIDSelecionado())
            {
                MostraStatus("Selecione um local para remover o tag RFID.");
                return;
            }
            if (dados.RemoveRFIDItemLocalSelecionado(ItemLocalentarianteSelecionado()))
            {
                EscondeMostra(LocalTagRemoveBt, true);
                labelTagLocalRFIDValue.Content = string.Empty;
            }
        }
        #endregion AssociacaoLocalRFID

        #region AssociacaoInvRFID
        private void CarregaInvSemRFID()
        {
            // Busco todos os inventariantes sem RFID
            listBoxInvSemRFID.ItemsSource = dados.InvsSemTagRFID;
            listBoxInvSemRFID.SelectedIndex = -1;
        }
        private void MostraAssociacaoInvRFID()
        {
            CarregaInvSemRFID();
            EscondeMostraThread(AssociacaoInvRFID);
            MostraTodosItensMenu(false);
        }
        private void EscondeAssociacaoInvRFID()
        {
            EscondeMostraThread(AssociacaoInvRFID, true);
            MostraTodosItensMenu();
            listBoxInvSemRFID.SelectedIndex = -1;
        }
        private object ItemInventarianteSelecionado()
        {
            return listBoxInvSemRFID.SelectedItem;
        }
        private bool TemInvSemRFIDSelecionado()
        {
            return (listBoxInvSemRFID.SelectedItem is Modelo.Pessoa);
        }
        private void LeTagInvRFID()
        {
            if (!TemInvSemRFIDSelecionado())
            {
                MostraStatus("Selecione um inventariante para aplicar o tag RFID.");
                return;
            }
            IniciaBuscaTagAssociacaoInvRFID();
        }
        private bool IniciaBuscaTagAssociacaoInvRFID()
        {
            if (!DispositivoAtivo())
            {
                MostraStatus(dados.MensagemDispositivoNaoEncontrado);
                return false;
            }
            ParaBuscaTagAssociacaoInvRFID();
            dispositivoAtual.OnRead += new ChangeEventHandler(AtualizaTagAssociacaoInvRFID);
            if (!IniciaLeitura(true))
                return false;
            return true;
        }
        private void ParaBuscaTagAssociacaoInvRFID()
        {
            if (DispositivoExiste())
                dispositivoAtual.OnRead -= new ChangeEventHandler(AtualizaTagAssociacaoInvRFID);
            ParaLeitura();
        }
        private void AtualizaTagAssociacaoInvRFID()
        {
            try
            {
                // Ao encontrar tags para a leitura
                ParaBuscaTagAssociacaoInvRFID();

                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke(() => AtualizaTagAssociacaoInvRFID());
                    return;
                }

                // Avalia a lista de tags encontradas
                foreach (RFIDTag item in dispositivoAtual.Memory.Tags)
                {
                    // Se a tag não está associada a um bem, local ou pessoa que está na memória do sistema
                    // Mesmo assim, ainda pode dar erro ao integrar com o servidor, se inserida a mesma tag em outro objeto
                    if (dados.ExisteEntidadeComEsteTagRFID(item.Id))
                    {
                        MostraStatus("Tag RFID encontrada já está vinculada.");
                        continue;
                    }
                    dados.AssociaTagInv(ItemInventarianteSelecionado(), item.Id);
                    AlteraTagAssociacaoInvRFIDBox(item.Id);
                    return;
                }
            }
            catch (Exception e)
            {
                MostraStatus("Erro ao tentar ler a Tag RFID - " + e.Message);
                Erro.Registra(e);
            }
        }
        private void AlteraTagAssociacaoInvRFIDBox(string texto)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraTagAssociacaoInvRFIDBox(texto));
                return;
            }
            texto = texto.Trim();
            labelTagInvRFIDValue.Content = texto;
            labelTagInvRFIDValue.UpdateLayout();
            if (texto.Length > 0)
                EscondeMostra(InvTagRemoveBt);
        }
        private void MudaTagAssociacaoInvRFIDSelecionada()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => MudaTagAssociacaoInvRFIDSelecionada());
                return;
            }
            string aux = dados.ObtemRFIDItemInvSelecionado(ItemInventarianteSelecionado()).Trim();
            labelTagInvRFIDValue.Content = aux;
            if (aux.Length == 0)
            {
                EscondeMostra(InvTagRemoveBt,true);
                return;
            }
            EscondeMostra(InvTagRemoveBt);
        }
        private void RemoveTagAssociacaoInvRFID()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => RemoveTagAssociacaoInvRFID());
                return;
            }
            if (!TemInvSemRFIDSelecionado())
            {
                MostraStatus("Selecione um inventariante para remover o tag RFID.");
                return;
            }
            if (dados.RemoveRFIDItemInvSelecionado(ItemInventarianteSelecionado()))
            {
                EscondeMostra(InvTagRemoveBt, true);
                labelTagInvRFIDValue.Content = string.Empty;
            }
        }
        #endregion AssociacaoInvRFID

        #region Autenticacao
        private async void AutenticarUsuario()
        {
            EscondeSenha();
            Mouse.OverrideCursor = Cursors.Wait;

            if (usertextBox.Text.Trim().Length == 0 || senhapasswordBox.Password.Trim().Length == 0)
            {
                MostraStatus("Usuário ou senha não podem ser vazios.");
                Mouse.OverrideCursor = null;
                MostraSenha();
                usertextBox.Focus();
                return;
            }

            MostraStatus("Validando licença...");
            ukm u = new ukm();
            var validaLicencaTask = Task<bool>.Factory.StartNew(() => u.ValidaLicenca());
            if (!await validaLicencaTask)
            {
                MostraLicencaInvalida();
                Mouse.OverrideCursor = null;
                MostraSenha();
                usertextBox.Focus();
                return;
            }

            MostraStatus("Autenticando usuário...");
            Auth au = new Auth();
            List<string> usuariosenha = ObtemUsuarioSenha();
            var validaUsuarioTask = Task<bool>.Factory.StartNew(() => au.ValidaUsuario(usuariosenha.ToArray()[0], usuariosenha.ToArray()[1]));
            if (!await validaUsuarioTask)
            {
                MostraStatus("Não foi possível autenticar no servidor. Verificar se o usuário e senha estão corretos.");
                MostraSenha();
                usertextBox.Focus();
                Mouse.OverrideCursor = null;
                return;
            }
            dados.TokenUsuario = au.token;
            dados.UsuarioAutenticado = new Modelo.Pessoa();
            dados.UsuarioAutenticado.login = usuariosenha.ToArray()[0];
            LimpaSenha();
            Mouse.OverrideCursor = null;
        }
        private List<string> ObtemUsuarioSenha()
        {
            if (!Dispatcher.CheckAccess())
            {
                return Dispatcher.Invoke(() => ObtemUsuarioSenha());
            }
            List<string> retorno = new List<string>();
            if (usertextBox.Text.Trim().Length == 0 || senhapasswordBox.Password.Trim().Length == 0)
                return retorno;
            retorno.Add(usertextBox.Text.Trim());
            retorno.Add(senhapasswordBox.Password.Trim());
            return retorno;
        }
        private void DesconectarUsuario()
        {
            dados.DesconectaUsuario();
            LimpaAutenticacao();
            MostraSenha();
        }
        private void MostraSenha()
        {
            EscondeMostraThread(senhaLabel);
            EscondeMostraThread(senhapasswordBox);
            EscondeMostraThread(senhaRet);
            EscondeMostraThread(FazerLoginBt);
            EscondeMostraThread(FazerLogoutBt, true);
        }
        private void EscondeSenha()
        {
            EscondeMostraThread(senhaLabel, true);
            EscondeMostraThread(senhapasswordBox, true);
            EscondeMostraThread(senhaRet, true);
            EscondeMostraThread(FazerLoginBt, true);
            EscondeMostraThread(FazerLogoutBt);
        }
        private void MostraBotaoLogout()
        {
            EscondeMostraThread(FazerLogoutBt);
        }
        private void EscondeBotaoLogout()
        {
            EscondeMostraThread(FazerLogoutBt, true);
        }
        private void LimpaSenha()
        {
            senhapasswordBox.Clear();
        }
        private void LimpaAutenticacao()
        {
            usertextBox.Clear();
            senhapasswordBox.Clear();
        }
        private void MostraLicencaInvalida()
        {
            EscondeMostraThread(LicencaInvalidaCanvas);
        }
        private void EscondeLicencaInvalida()
        {
            EscondeMostraThread(LicencaInvalidaCanvas, true);
        }
        #endregion Autenticacao

        #region Sincronia de dados
        private async void CarregaDadosServidor()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            MostraStatus("Recebendo dados...");
            Task<bool> baixarDadostask = Task.Factory.StartNew(() => dados.BaixarDados());
            if (!await baixarDadostask)
            {
                MostraStatus("Falha ao buscar dados no servidor. Autenticar usuário ou token do usuário é inválido ou conexão com o servidor falou. Verificar se as configurações estão corretas.");
                Mouse.OverrideCursor = null;
                return;
            }
            // Tenta buscar e salvar novos dados da nuvem
            MostraStatus("Salvando dados...");
            Task<bool> salvaDadostask = Task.Factory.StartNew(() => dados.SalvaDadosDisco());
            if (!await salvaDadostask)
            {
                MostraStatus("Falha ao salvar dados no disco.");
                Mouse.OverrideCursor = null;
                return;
            }
            MostraStatus("Dados recebidos e armazenados. Pessoas:" + dados.Pessoas.Count.ToString() + " Locais: " + dados.Locais.Count.ToString() + " Bens: " + dados.Bens.Count.ToString());
            Mouse.OverrideCursor = null;
        }
        private async void EnviaDados()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            MostraStatus("Enviando dados...");
            Task<bool> enviarDadosNuvemtask = Task.Factory.StartNew(() => dados.EnviarDadosNuvem());
            if (!await enviarDadosNuvemtask)
            {
                MostraStatus("Falha enviar dados para o servidor. Conexão com o servidor falhou, token do usuário é inválido, usuário sem permissão ou verificar se as configurações estão corretas.", 8);
                Mouse.OverrideCursor = null;
                return;
            }
            Task<bool> excluiDadosInventariotask = Task.Factory.StartNew(() => dados.ExcluiDadosInventario());
            MostraStatus("Dados enviados ao servidor.");
            // Limpa do arquivo
            if (!await excluiDadosInventariotask)
            {
                MostraStatus("Dados de invetarios não foram removidos, podem gerar registros duplicados se enviados novamente. Remover o arquivo InventarioBem.");
                Mouse.OverrideCursor = null;
                return;
            }
            MostraStatus("Dados locais, de invetarios e associações de tag RFID, foram removidos.");
            Mouse.OverrideCursor = null;
        }
        private void MostraSincronia()
        {
            MostraTodosItensMenu(false);
            EscondeMostraThread(Servidor);
        }
        private void EscondeSincronia()
        {
            EscondeMostraThread(Servidor, true);
            MostraTodosItensMenu();
        }
        #endregion Sincronia de dados

        #region Configuracao
        private void CarregaConfiguracao()
        {
            if (!dados.CarregaPortaDOT900())
                MostraStatus(dados.MensagemErroConfigDOT900Ler);
            if (dados.portaDOT900.Length == 0)
                MostraStatus(dados.MensagemConfigDOT900Invalido);
        }
        private void SalvaConfiguracao()
        {
            string aux = PortaDOT900textBox.Text.Trim();
            int comaux = 0;
            if (aux.Length == 0 || !int.TryParse(aux, out comaux) || comaux <= 0)
            {
                MostraStatus(dados.MensagemConfigDOT900Invalido);
                return;
            }
            dados.portaDOT900 = aux;
            if (!dados.AlteraPortaDOT900())
            {
                MostraStatus("Não foi possivel alterar a porta do dispositivo DOT 900.");
                return;
            }
            MostraStatus("Configuração alterada com sucesso.");
        }
        private void MostraConfiguracao()
        {
            // Carrega dados de configuração
            CarregaConfiguracao();
            // Altera componente de tela
            PortaDOT900textBox.Text = dados.portaDOT900.Trim();
            // Mostra a tela
            EscondeMostraThread(Config);
            // Esconde o botão de abertura da tela
            MostraTodosItensMenu(false);
            // foco no primeiro item de config
            PortaDOT900textBox.Focus();
            PortaDOT900textBox.SelectAll();
        }
        private void EscondeConfiguracao()
        {
            // Esconde a tela
            EscondeMostraThread(Config, true);
            // Mostra o botão de abertura da tela
            MostraTodosItensMenu();
        }
        #endregion Configuracao

        #region Licenca
        private async void MostraLicenca()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            MostraTodosItensMenu(false);
            ukm u = new ukm();
            var serialTask = Task<string>.Factory.StartNew(() => u.Serial);
            var chaveTask = Task<string>.Factory.StartNew(() => u.Chave);
            var versaoTask = Task<string>.Factory.StartNew(() => u.Versao);
            try
            {
                await serialTask;
                labelSerialValor.Content = serialTask.Result;
            }
            catch (Exception e)
            {
                MostraStatus("Não foi possível obter o valor do serial.");
                Erro.Registra(e);
            }
            try
            {
                await chaveTask;
                labelMachinaValor.Content = chaveTask.Result;
            }
            catch (Exception e)
            {
                MostraStatus("Não foi possível obter o valor da chave.");
                Erro.Registra(e);
            }
            try
            {
                await versaoTask;
                labelVersaoValor.Content = versaoTask.Result;
            }
            catch (Exception e)
            {
                MostraStatus("Não foi possível obter o valor da versão.");
                Erro.Registra(e);
            }
            EscondeMostraThread(serial);
            Mouse.OverrideCursor = null;
        }
        private void EscondeLicenca()
        {
            EscondeMostraThread(serial, true);
            MostraTodosItensMenu();
        }
        private void CopiaSerial()
        {
            ToClipboard(labelSerialValor.Content.ToString());
            MostraStatus("Serial copiado para a memória.");
        }
        private void CopiaChave()
        {
            ToClipboard(labelMachinaValor.Content.ToString());
            MostraStatus("Chave copiada para a memória.");
        }
        #endregion Licenca

        #region StatusBar
        private void MostraStatus(string mensagem, double segundos = 3, double tamanhofonte = 12)
        {
            AlteraStatusThread(mensagem, tamanhofonte);
            if (segundos <= 0)
                return;
            LimpaStatusTimerThread(segundos);
        }
        private void AlteraStatusThread(string mensagem, double tamanho = 12)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => AlteraStatus(mensagem, tamanho));
                return;
            }
            AlteraStatus(mensagem, tamanho);
        }
        private void AlteraStatus(string mensagem, double tamanho = 12, bool agregamsg = false)
        {
            if (agregamsg)
            {
                string aux = string.Empty;
                aux = StatusMsg.Text;
                if (aux.Trim().Length > 0)
                    StatusMsg.Text = mensagem + '\n' + aux;
                else
                    StatusMsg.Text = mensagem;
            }
            else
                StatusMsg.Text = mensagem;
            StatusMsg.FontSize = tamanho;
            StatusMsg.UpdateLayout();
            AlteraStatusHistorico(mensagem);
            Console.WriteLine(mensagem);
        }
        private void AlteraStatusHistorico(string mensagem)
        {
            string aux = string.Empty;
            aux = StatusBarBox.Text;
            StatusBarBox.Text = mensagem + '\n' + '\n' + aux;
            StatusBarBox.UpdateLayout();
        }
        private void ConfigStatusTimerThread()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => ConfigStatusTimer());
                return;
            }
            ConfigStatusTimer();
        }
        private void ConfigStatusTimer()
        {
            statusTimer.Elapsed += LimpaStatusEvent;
            statusTimer.Interval = 10000;
            statusTimer.Start();
        }
        private void LimpaStatusTimerThread(double segundos = 3)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => LimpaStatusTimer(segundos));
                return;
            }
            LimpaStatusTimer(segundos);
        }
        private void LimpaStatusTimer(double segundos = 3)
        {
            if (segundos <= 0)
                segundos = 10;
            statusTimer.Interval = segundos * 1000;
        }
        private void LimpaStatusEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            LimpaStatusThread();
        }
        private void LimpaStatusThread()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => LimpaStatus());
                return;
            }
            LimpaStatus();
        }
        private void LimpaStatus()
        {
            StatusMsg.Text = string.Empty;
        }
        #endregion StatusBar

        #region Metodos auxiliares
        private void EscondeMostraThread(Object o, bool esconde = false)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(() => EscondeMostraThread(o, esconde));
                return;
            }
            EscondeMostra(o, esconde);
        }
        private void EscondeMostra(Object o, bool esconde = false)
        {
            // assertivas de entrada
            if (o == null)
                return;

            if (esconde)
            {
                if (o is Shape)
                {
                    ((Shape)o).Visibility = Visibility.Hidden;
                    return;
                }
                if (o is Canvas)
                {
                    ((Canvas)o).Visibility = Visibility.Hidden;
                    return;
                }
                if (o is TextBlock)
                {
                    ((TextBlock)o).Visibility = Visibility.Hidden;
                    return;
                }
                ((Control)o).Visibility = Visibility.Hidden;
                return;
            }
            if (o is Shape)
            {
                ((Shape)o).Visibility = Visibility.Visible;
                return;
            }
            if (o is Canvas)
            {
                ((Canvas)o).Visibility = Visibility.Visible;
                return;
            }
            if (o is TextBlock)
            {
                ((TextBlock)o).Visibility = Visibility.Visible;
                return;
            }
            ((Control)o).Visibility = Visibility.Visible;
        }
        private void ToClipboard(string dados)
        {
            Clipboard.SetText(dados);
        }
        #endregion  Metodos auxiliares

        #endregion App
    }
}
